let http = require('http');

let port = 3000;

let server = http.createServer(function(request, response) {
	if(request.url == '/login'){
		response.writeHead(200, {'Content-type': 'text/plain'});
		response.end('Welcome to the login page.');
	} else {
		response.writeHead(404, {'Content-type': 'text/plain'});
		response.end(`I'm sorry the page you are looking for cannot be found.`);
	};
});

server.listen(port);

console.log(`Server is running at localhost:${port}`);
